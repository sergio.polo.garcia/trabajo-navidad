#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio_ext.h>
#include <unistd.h>

#define MAX 50


/*****		1. Pregunta un número entero al usuario e imprímelo en pantalla. *****/ 

int numero_entero(){ 

	int entero; 

		printf("Dime un número entero: ");
		scanf("%i", &entero);

		printf("El numero es: %i \n\n", entero);

	return entero;
}

 /*****		2. Pregunta un número real al usuario e imprímelo en pantalla. *****/

int numero_real(){

	double real;

		printf("Dime un número real: ");
		scanf("%lf", &real);

		printf("El numero es: %.3lf \n\n", real); 

	return real;
}

/*****		3. Pregunta un número entero entre 32 y 128 al usuario e imprímelo como carácter y como número. *****/

int numero_rango(){

	int num_i;
	char num_ch;

		printf("Dime un numero entre el 32 y el 128: ");
		scanf(" %i", &num_i);
 	
		printf("El numero es: %i \n", num_i);
	
		num_ch = num_i;
		printf("Como caracter es: %c \n\n", num_i);
	
	return num_ch, num_i;
}


/*****		4. Pregunta al usuario un número hexadecimal, guárdalo en un array de caracteres e imprímelo. No aceptes ningún caracter que no pertenezca al hexadecimal. *****/

void numero_hexadecimal(){
	
	char hexa[MAX];

		printf("Dime un numero hexadecimal: ");
		scanf(" %[0-9a-f-A-F]", hexa); 
	
	
		printf("El numero es: %s\n", hexa);
}

/*****		5. Borra la pantalla usando una secuencia de escape ANSI. *****/

void borrar_pantalla(){
	
	printf("\033[2J");
}



/*****		6. Pregunta dos operandos e imprímelos junto con la suma. <op1> + <op2> = <resultado>. Imprime los operandos en azul brillante. y el resultado en verde normal. *****/

int preguntar_op(){

	static int nop = 0;
	int op;
	
		printf("Inserte operando %i: ", ++nop);
		scanf("%i", &op);

	return op;

}

int imprimir_suma(int op1, int op2, int result){

	result = op1 + op2;	

		printf("\x1b[34m%i + \x1b[34m%i = \x1b[32m%i \n", op1, op2, result); 

	return result;
}


/*****		7. Guarda la posición del cursor. Imprime HOLA. Situa el cursor en la línea de arriba, encima de la H de HOLA. Borra la pantalla desde el inicio hasta el cursor usando una secuencia de escape ANSI. Vuelve a poner el cursor en la H de HOLA. *****/



/*****		8. Lee una línea entera usando fgets. Elimina el '\n' final substituyéndolo por un '\0'. Usa strlen para calcular la longitud de la línea. Imprímela usando puts *****/


void leer_linea(){

	char line[MAX];
		__fpurge (stdin);

		printf("Dime una frase: ");
		fgets(line, MAX, stdin); 

	char scan_line = strlen (line);
	line[scan_line-1] = '\0'; 

		printf("la frase es: \n\n");
		puts(line);
}


/*****		9. Pregúntale un número al usuario e imprime su valor hexadecimal usando letras mayúsculas. *****/

void preguntar_hexadecimal(){

	int result_hex;

	printf("Dime un numero en decimal: ");
	scanf("%i", &result_hex);

	printf("El numero en Hexadecimal es  = %X\n\n", result_hex);
	
}


/*****		10. Imprime la dirección de memoria de una variable. *****/

void imprimir_direccion(){

int variable;
	printf("La direccion de memoria es: %p \n\n", &variable);
}


int main(){

	int op1, op2, result;

	borrar_pantalla();

	numero_entero();

	numero_real();

	numero_rango();

	numero_hexadecimal();

	leer_linea();
	
	preguntar_hexadecimal();

	imprimir_direccion();

	op1 = preguntar_op();
	op2 = preguntar_op();
	imprimir_suma(op1, op2, result);	
	
return EXIT_SUCCESS;

}
